let fruits = [
    {id: 1, title: 'Raspberry', price: 20, img: 'images/berries.jpg'},
    {id: 2, title: 'Blueberries', price: 40, img: 'images/blueberries.jpg'},
    {id: 3, title: 'Strawberries', price: 60, img: 'images/strawberries.jpg'},
]

const toHTML = fruit => `
    <div class="card">
        <div class="card-body">
            <div class="card-img">
                <img src="${fruit.img}" class="card-img-top" alt="${fruit.title}">
            </div>
            <h5 class="card-title">${fruit.title}</h5>
            <a href="#" class="btn btn-buy" data-btn="price" data-id="${fruit.id}">Show price</a>
            <a href="#" class="btn btn-clear" data-btn="remove" data-id="${fruit.id}">Clear</a>
        </div>
    </div>
`

function render() {
    const html = fruits.map(fruit => toHTML(fruit)).join('');
    document.querySelector('#fruits').innerHTML = html;
}

render();

document.addEventListener('click', event => {
    event.preventDefault();
    const btnType = event.target.dataset.btn;
    const id = +event.target.dataset.id;

    if (btnType === 'price') {
        const fruit = fruits.find(f => f.id === id);
        const priceModal = M.modal({
            title: 'Modal title',
            closable: true,
            width: '400px',
            onClose() {
                priceModal.destroy();
            },
            footerButtons: [
                {text: 'Ok', type: 'ok', handler() {
                    priceModal.close();
                }},
            ]
        });
        
        priceModal.setContent(`
            <p>Price ${fruit.title}: <strong>${fruit.price}$</strong></p>
        `);
        priceModal.open();
    } else if (btnType === 'remove') {
        const fruit = fruits.find(f => f.id === id);
        M.confirm({
            title: 'Remove',
            content: `<p>You delete: <strong>${fruit.title}</strong></p>`
        }).then(() => {
            fruits = fruits.filter(f => f.id !== id);
            render();
        }).catch(() => {
            console.log('Cancel');
        })
    }
});