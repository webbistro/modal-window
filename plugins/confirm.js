M.confirm = function(options) {
    return new Promise((resolve, reject) => {
        const modal = M.modal({
            title: options.title,
            width: '400px',
            closable: false,
            content: options.content,
            onClose() {
                modal.destroy();
            },
            footerButtons: [
                {text: 'Cancel', type: 'cancel', handler() {
                    modal.close();
                    reject();
                }},
                {text: 'Remove', type: 'remove', handler() {
                    modal.close();
                    resolve();
                }},
            ]
        });
        setTimeout(() => modal.open(), 100);
    })
}