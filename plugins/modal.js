const M = {};

Element.prototype.appendAfter = function(element) {
    element.parentNode.insertBefore(this, element.nextSibling);
}

function noop() {}

function createModalFooter(buttons = []) {
    if (buttons.length === 0) {
        return document.createElement('div');
    }

    const wrap = document.createElement('div');
    wrap.classList.add('modal-footer');

    buttons.forEach(btn => {
        const $btn = document.createElement('button');
        $btn.textContent = btn.text;
        $btn.classList.add('btn');
        $btn.classList.add(`btn-${btn.type || 'default'}`);
        $btn.onclick = btn.handler || noop;

        wrap.appendChild($btn);
    })

    return wrap;
}

function createModal(options) {
    const DEFAULT_WIDTH = '600px';
    const modal = document.createElement('div');
    modal.classList.add('modal-dialog');
    modal.insertAdjacentHTML('afterbegin', `
        <div class="modal-overlay" data-close="true">
            <div class="modal-body" style="width: ${options.width || DEFAULT_WIDTH}">
                <div class="modal-header">
                    <div class="modal-header__title">${options.title || ''}</div>
                    ${options.closable ? `<div class="modal-header__close" data-close="true"></div>` : ''}
                </div>
                <div class="modal-content" data-content>
                    ${options.content || ''}
                </div>
            </div>
        </div>
    `);

    const footer = createModalFooter(options.footerButtons);
    footer.appendAfter(modal.querySelector('[data-content]'));
    document.body.appendChild(modal);
    return modal;
}

M.modal = function(options) {
    const ANIMATION_SPEED = 200;
    const $modal = createModal(options);
    let closing = false;
    let destroyed = false; 

    const modal = {
        open() {
            if (destroyed) return;

            !closing && $modal.classList.add('open');
        },
        close() {
            closing = true;
            $modal.classList.remove('open');
            $modal.classList.add('hide');
            setTimeout(() => {
                $modal.classList.remove('hide');
                closing = false;
                if (typeof options.onClose === 'function') {
                    options.onClose();
                }
            }, ANIMATION_SPEED);
        },
        destroy() {}
    }

    const listener = event => {
        if (event.target.dataset.close) {
            modal.close();
        }
    }

    $modal.addEventListener('click', listener);

    return Object.assign(modal, {
        destroy() {
            $modal.parentNode.removeChild($modal);
            $modal.removeEventListener('click', listener);
            destroyed = true;
        },
        setContent(html) {
            $modal.querySelector('[data-content]').innerHTML = html;
        }
    });
}